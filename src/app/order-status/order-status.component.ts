import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { OrderDataService } from '../order-data.service';

@Component({
  selector: 'app-order-status',
  templateUrl: './order-status.component.html',
  styleUrls: ['./order-status.component.css'],
})
export class OrderStatusComponent implements OnInit {
  @Input() oid: any;
  @Input() order: any;

  constructor(
    public orderDataService: OrderDataService,
    private route: ActivatedRoute
  ) {}

  ngOnInit(): void {
    console.log('OrderStatusComponent', this.oid);

    // console.log('osid',this.oid);
    this.order = this.orderDataService.getOrderData(this.oid);
  }
}
