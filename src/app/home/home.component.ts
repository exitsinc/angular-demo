import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { OrderDataService } from '../order-data.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
})
export class HomeComponent implements OnInit {
  @Input() oid: any;
  @Input() order: any;

  constructor(
    public orderDataService: OrderDataService,
    private route: ActivatedRoute
  ) {}

  ngOnInit(): void {
    this.route.paramMap.subscribe((params) => {
      console.log('oid', params.get('id'));
      this.oid = params.get('id') || 2;
    });
  }
}
