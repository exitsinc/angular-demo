import { Injectable } from '@angular/core';
import _ from 'lodash';

@Injectable({
  providedIn: 'root',
})
export class OrderDataService {
  constructor() {}

  orderData: Array<{
    id: number;
    productName: string;
    grade: string;
    slit: number;
    source: string;
    lot: string;
    status: number;
  }> = [
    {
      id: 1,
      productName: 'Concrete M - Sand ',
      grade: 'Very fine (2.3 to 3.2.)',
      slit: 12,
      source: 'Granite stone',
      lot: '350 tons (35 tons/truck)',
      status: 1,
    },
    {
      id: 2,
      productName: 'Rubble M - Sand ',
      grade: 'Coarse (5.3 to 8.2.)',
      slit: 4,
      source: 'Black  stone',
      lot: '350 tons (35 tons/truck)',
      status: 2,
    },
    {
      id: 3,
      productName: 'Rubble M - Sand ',
      grade: 'Coarse (5.3 to 8.2.)',
      slit: 4,
      source: 'Black  stone',
      lot: '350 tons (35 tons/truck)',
      status: 3,
    },
    {
      id: 4,
      productName: 'Rubble M - Sand ',
      grade: 'Coarse (5.3 to 8.2.)',
      slit: 4,
      source: 'Black  stone',
      lot: '350 tons (35 tons/truck)',
      status: 4,
    },
  ];

  public getOrderData(
    oid: any = 1
  ): Array<{
    id: number;
    productName: string;
    grade: string;
    slit: number;
    source: string;
    lot: string;
    status: number;
  }> {
    // return this.orderData;
    return _.find(this.orderData, ['id', Number(oid)]);
  }
}
