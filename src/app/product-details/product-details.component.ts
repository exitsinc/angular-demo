import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { OrderDataService } from '../order-data.service';

@Component({
  selector: 'app-product-details',
  templateUrl: './product-details.component.html',
  styleUrls: ['./product-details.component.css'],
})
export class ProductDetailsComponent implements OnInit {
  @Input() oid: any;
  @Input() order: any;

  constructor(
    public orderDataService: OrderDataService,
    private route: ActivatedRoute
  ) {}

  ngOnInit(): void {
    console.log('ProductDetailsComponent',this.oid);
    this.order = this.orderDataService.getOrderData(this.oid);
  }
}
